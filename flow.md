### main flow
1. write spec
1. write rtl
1. pre syn sim
1. syn
1. post syn sim
1. formality
1. io pad sim



### dc flow

1. remove_design -design
1. set_svf top.svf
1. set library info
1. set dc library
1. check setup information
1. check_library
1. check_tlu_plus_files
1. analyze -format verilog
1. elaborate $DESIGN_NAME
1. uniquify
1. ungroup -all
1. link
1. check_design > ../rep/check_design
1. write -hierarchy -f ddc -out ../unmapped/$DESIGN_NAME.ddc
1. source dc_sdc_v2.tcl > sdc.log
1. check_timing > ../rep/check_timing

1. group_path -to [all_outputs] -name REG2OUT
1. group_path -from [all_inputs] -name IN2REG
1. group_path -from [all_inputs] -to [all_outputs] -name COMBO

1. compile_ultra -no_autoungroup

1. compile_ultra -no_autoungroup -incr
1. #compile -scan

1. report_qor > ../rep/rep_qor
1. report_constraints -all > ../rep/rep_consitraints
1. report_timing > ../rep/rep_timing
1. report_area > ../rep/rep_area
1. report_power > ../rep/rep_power
1. write -hierarchy -format ddc -output ../out/$DESIGN_NAME.mapped.ddc
1. write -hierarchy -format verilog -output ../out/$DESIGN_NAME.mapped.v
1. write_sdc ../out/$DESIGN_NAME.mapped.sdc
1. write_sdf ../out/$DESIGN_NAME.mapped.sdf
1. ##fm
1. set_svf off

### icc flow
1.
1.
1.
1.
1.
