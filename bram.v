
//  vcs -o bram  +lint=TFIPC-L  -full64 -debug_access+r -debug_access+r+w  -notice  +incdir+../rtl   +sdfverbose  +neg_tchk  -negdelay -top  bram_tb   bram.v


module  bram
(
    WCLK,
    RESETN,

    WE,
    ADDR,
    DIN,
    DOUT
);

input          WCLK;
input          RESETN;
input          WE;
input  [7:0]   ADDR;
input          DIN;
output         DOUT;

wire           WCLK;
wire           RESETN;
wire           WE;
wire   [7:0]   ADDR;
wire           DIN;
reg            DOUT;



reg  [149:0]   memory;

always @(posedge WCLK )
begin
    if(RESETN) begin
        if(WE)
        begin
            memory[ADDR] <= DIN;
        end
    end else begin
        memory <= 150'h0;
    end
end

always @(*)
begin
    DOUT = memory[ADDR];
end
endmodule








module  bram_tb
(
);

reg            WCLK;
reg            RESETN;
reg            WE;
reg    [7:0]   ADDR;
reg            DIN;
wire           DOUT;
reg    [3:0]   r;


bram    ram(
    .WCLK    (WCLK),
    .RESETN  (RESETN),

    .WE      (WE),
    .ADDR    (ADDR),
    .DIN     (DIN),
    .DOUT    (DOUT)
);


initial begin
    WCLK  = 0;

    forever
    begin
#5
        WCLK = !WCLK;
    end
end


initial begin
    RESETN = 0;
#100
    RESETN = 1;

#10
    ram.memory = 148'h0011223344556677001122334455667700112;

#20
    ADDR = 8'h10;
#20
    $display("%x" , DOUT);
    r[0] = DOUT;


#20
    ADDR = 8'h20;
#20
    $display("%x" , DOUT);
    r[1] = DOUT;

#20
    ADDR = 8'h4;
#20
    $display("%x" , DOUT);
    r[2] = DOUT;

#20
    ADDR = 8'ha0;
#20
    $display("%x" , DOUT);
    r[3] = DOUT;


#20
    if( r === 4'bx100)
        $display("ok");
    else
        $display("error");

#100
    $finish(0);
end

endmodule
