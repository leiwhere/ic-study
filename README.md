# ic-study

[Verilog动态截取固定长度语法+:和-:](https://blog.csdn.net/whik1194/article/details/113874073)

[HDLBits 系列（0）专题目录](https://reborn.blog.csdn.net/article/details/103805729)

[HDLBits ](https://hdlbits.01xz.net/wiki/Main_Page)

[全定制设计——SRAM单元电路](https://www.docin.com/p-1924787882.html)

[Memory-Compiler使用入门介绍](https://wenku.baidu.com/view/4e475d5f32687e21af45b307e87101f69e31fb70.html)

[ARM的memory Compiler总结](https://blog.csdn.net/lyfwill/article/details/81330081)

[流片前的check list](https://www.docin.com/p-884717443.html)

[ICC图文流程——(一)数据准备Data Setup](https://blog.csdn.net/weixin_46752319/article/details/107252336)

[IC芯片命名规则大全](https://wenku.baidu.com/view/ee59e6297375a417866f8f76.html)

[基于Synopsys的ASIC设计流程系列:SDC概述](https://zhuanlan.zhihu.com/p/60229579)

[synopsys SCL服务运行与License获取时的常见错误](https://www.fermitech.com.cn/wiki/doku.php?id=atk:scl%E6%9C%8D%E5%8A%A1%E8%BF%90%E8%A1%8C%E4%B8%8Elicense%E8%8E%B7%E5%8F%96%E6%97%B6%E7%9A%84%E5%B8%B8%E8%A7%81%E9%94%99%E8%AF%AF)


[IC Compiler II（ICC II）后端设计流程——超详细](https://www.freesion.com/article/87891283744/#IC_Compiler_II_Library_Manager_76)

[DC_ICC2](https://blog.csdn.net/weixin_41464428)

[scl_keygen](http://www.ddooo.com/softdown/123032.htm)

[各种feature](https://alllicenseparser.com/engineering-db/flexlm/snpslmd/index.html)


[Synopsys系列软件License制作——亲测可用](https://blog.csdn.net/qq_36215315/article/details/107159384)

[DC综合脚本中文详细解释](https://blog.csdn.net/weixin_38407219/article/details/89448667)

[DC(Design Compiler)简明教程](https://tech.hqew.com/fangan_1649208)

[Design Compiler](https://www.cnblogs.com/IClearner/tag/DC/)

[Synopsys逻辑综合及DesignCompiler的使用](https://blog.csdn.net/qq_42759162/article/details/105541240)


[后端设计流程](https://blog.csdn.net/qq_42759162/article/details/108461592)


[干货满满--数字后端设计及ICC教程整理](https://mp.weixin.qq.com/s/kz1J8HGEgzWxoe-csWijjg)


[ICC的基本使用步骤](https://wenku.baidu.com/view/e39a73b8c77da26925c5b061.html)


[ICC 命令集](https://mp.weixin.qq.com/s?__biz=MzIyMjc3MDU5Mw==&mid=2247484094&idx=1&sn=120c943e284c8fcb13e2924df2b39071&chksm=e8292670df5eaf66d5e26d52efd31825ed50c0a02b21e405723c8687dceb5cd26c414327e4e1&scene=21#wechat_redirect)

[Design Compiler](https://blog.csdn.net/l471094842/category_9522360.html)

[DFT 相关文档](https://max.book118.com/html/2019/0811/5311104310002113.shtm)