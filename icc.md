# regex 实验

-------

###  说明

本实验学习如何使用 synopsys 的工具进行 ic 设计

本文档随时修订


###  代码结构说明

> 基于如下考虑：

-  适合大规模多人协作
-  对 版本管理工具 git 友好
-  对 build 系统友好
-  功能划分清楚
-  容易阅读
-  方便移植到不同的开发工具
-  方便移植到不同工艺

> 采用以下的目录结构

```
code root   
    |
    |--scripts   存放前后端工作需要的各种脚本 (脚本说明如下)
    |--rtls      源代码目录
    |--work      工作目录(可 rm -rf * 清空)

```


> 另外scripts目录里的脚本文件遵循下面的命名规则

-  文件分两种 ， 一种是 需要 执行 的 命令 ， 扩展名为 tcl ， 另一种为 保存 配置 相关的 信息 ， 扩展名为 setup
-  命令文件名格式如下   主序号(2位数字)_次序号(2位数字)_功能说明.tcl

    例如 : ` 01_01_InitialDesign.tcl`


###  工作步骤记录

-  功能验证
    1) 搭建验证平台
    1) 编写测试用例
    1) 验证芯片的功能和时序

-  逻辑综合
    1) 将RTL代码综合成门级网表

-  形式验证
    1) 验证门级网表和RTL的一致性

-  物理设计,布局布线
    1) 完成自动布局布线
    1) 规划芯片的大小
    1) IO的位置
    1) 电源规划
    1) Initial design
    1) Floorplan
    1) Placement
    1) CTS
    1) POST CTS
    1) Route
    1) Check and Fix Physical DRC Violations
    1) antenna fix
    1) save_mw_cel
    1) save GDS
    1) save Gate-level Netlist for LVS

-  DRC和LVS验证
    1) DRC（设计规则验证）
    1) LVS（电路和版图一致性检查）

-  电路反标后仿
    1) 反标sdf文件，完成功能验证

-  时序分析
    1) 进行时序分析

-  流片,封装

-  芯片测试
    1) 设计电路板
    1) 搭建测试平台
    1) 完成板级测试
