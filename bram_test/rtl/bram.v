`timescale 1ns / 1ps
module  bram
(
    WCLK,
    RESETN,

    WE,
    ADDR,
    DIN,
    DOUT
);

input          WCLK;
input          RESETN;
input          WE;
input  [7:0]   ADDR;
input  [7:0]   DIN;
output         DOUT;

wire           WCLK;
wire           RESETN;
wire           WE;
wire   [7:0]   ADDR;
wire   [7:0]   DIN;
reg    [7:0]   DOUT;



reg  [7:0]   memory [147:0];

integer idx;

always @(posedge WCLK )
begin
    if(RESETN) begin
        if(WE)
        begin
            memory[ADDR] <= DIN;
        end
    end else begin
      	for(idx=0; idx<148; idx=idx+1)
       	begin
       	    memory[idx] <= 32'h0;
       	end
    end
end

always @(*)
begin
    DOUT = memory[ADDR];
end
endmodule



