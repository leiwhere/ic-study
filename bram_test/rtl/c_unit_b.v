`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: LD TECH
// Engineer: leiwhere
// 
// Create Date: 12/05/2021 12:09:23 AM
// Design Name: 
// Module Name: c unit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module LD_c_unit(
    clk,
    resetn,

    bram_a_clk,
    bram_a_addr,
    bram_a_wdata,
    bram_a_rdata,
    bram_a_en,
    bram_a_we,
    bram_a_rstn,

    bram_b_clk,
    bram_b_addr,
    bram_b_wdata,
    bram_b_rdata,
    bram_b_en,
    bram_b_we,
    bram_b_rstn,

    s_mode,
    s_valid,
    s_stream,
    s_last,

    founded,
    vid,
    value
);

input           clk;
input           resetn;

input  [   5:0] s_mode;
input           s_valid;
input  [1023:0] s_stream;
input           s_last;

output          founded;
output [   5:0] vid;
output [  31:0] value;


input           bram_a_clk;
input  [ 7:0]   bram_a_addr;
input  [31:0]   bram_a_wdata;
output [31:0]   bram_a_rdata;
input           bram_a_en;
input  [   3:0] bram_a_we;
input           bram_a_rstn;

input           bram_b_clk;
input  [ 7:0]   bram_b_addr;
input  [31:0]   bram_b_wdata;
output [31:0]   bram_b_rdata;
input           bram_b_en;
input  [   3:0] bram_b_we;
input           bram_b_rstn;



wire            bram_a_clk;
wire   [ 7:0]   bram_a_addr;
wire   [31:0]   bram_a_wdata;
reg    [31:0]   bram_a_rdata;
wire            bram_a_en;
wire   [   3:0] bram_a_we;
wire            bram_a_rstn;

wire            bram_b_clk;
wire   [ 7:0]   bram_b_addr;
wire   [31:0]   bram_b_wdata;
reg    [31:0]   bram_b_rdata;
wire            bram_b_en;
wire   [   3:0] bram_b_we;
wire            bram_b_rstn;

wire            clk;
wire            resetn;

wire   [ 5:0]   s_mode;
wire            s_valid;
wire   [1023:0] s_stream;
wire            s_last;

reg             founded;
reg    [  5:0]  vid;
wire   [  31:0] value;



reg    [31:0]   memory_32 [0:36];  // 128 bytes sign  , 16 bytes keep  , 4 bytes value
wire   [ 7:0]   memory_8  [0:147]; // 128 bytes sign  , 16 bytes keep  , 4 bytes valvalueue
wire   [2047:0] read_mem;


wire   [1023:0] sign;
wire   [ 127:0] keep;
reg    [ 127:0] r_tmp;


integer idx;


always @( posedge bram_a_clk )
begin
    if(resetn)
    begin
	if(bram_a_en)
	begin
        	if(bram_a_we[0]) begin
		    memory_32[bram_a_addr[7:2]][7:0] <= bram_a_wdata[7:0];
        	end

        	if(bram_a_we[1]) begin
		    memory_32[bram_a_addr[7:2]][15:8] <= bram_a_wdata[15:8];
        	end

        	if(bram_a_we[2]) begin
		    memory_32[bram_a_addr[7:2]][23:16] <= bram_a_wdata[23:16];
        	end

        	if(bram_a_we[3]) begin
		    memory_32[bram_a_addr[7:2]][31:24] <= bram_a_wdata[31:24];
        	end
	
		bram_a_rdata <= memory_32[bram_a_addr[7:2]];
	end

     end else begin
      	for(idx=0; idx<37; idx=idx+1)
       	begin
       	    memory_32[idx] <= 32'h0;
       	end
     end
end

always @( posedge bram_b_clk )
begin
    if(resetn)
    begin
	if(bram_b_en)
	begin
		/*
        	if(bram_b_we[0]) begin
		    memory_32[bram_b_addr[7:2]][7:0] <= bram_b_wdata[7:0];
        	end

        	if(bram_b_we[1]) begin
		    memory_32[bram_b_addr[7:2]][15:8] <= bram_b_wdata[15:8];
        	end

        	if(bram_b_we[2]) begin
		    memory_32[bram_b_addr[7:2]][23:16] <= bram_b_wdata[23:16];
        	end

        	if(bram_b_we[3]) begin
		    memory_32[bram_b_addr[7:2]][31:24] <= bram_b_wdata[31:24];
        	end
		*/
	
		bram_b_rdata <= memory_32[bram_b_addr[7:2]];
	end

     end
end


endmodule 



