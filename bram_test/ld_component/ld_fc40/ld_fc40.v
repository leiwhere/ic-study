`timescale 1ns/1ps

module LDFC_DFFRQ(Q, CK, D, R);
output Q;
input  D, CK, R;

reg Q; // add reg
always @(posedge CK or posedge R)
begin
    if(R)
    begin
        Q <= 1'b0;
    end else begin
        Q <= D;
    end
end

endmodule


module LDFC_OR2 (Y, A, B);
output Y;
input A, B;

  or I0(Y, A, B);
endmodule


module LDFC_XOR2 (Y, A, B);
output Y;
input A, B;

  xor I0(Y, A, B);
endmodule


module LDFC_AND2 (Y, A, B);
output Y;
input A, B;

  and I0(Y, A, B);
endmodule


module LDFC_INV (Y, A);
output Y;
input A;

  not  I0(Y, A);
endmodule


module LDFC_MUX2 (Y, A, B, S0);
output Y;
input A, B, S0;

  assign Y = S0 ? B : A ;
endmodule
