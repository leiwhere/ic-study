`timescale 1ns / 1ps

module  bram_tb
(
);

reg            WCLK;
reg            RESETN;
reg            WE;
reg    [7:0]   ADDR;
reg    [7:0]   DIN;
wire   [7:0]   DOUT;
reg    [3:0]   r;


bram    ram(
    .WCLK    (WCLK),
    .RESETN  (RESETN),

    .WE      (WE),
    .ADDR    (ADDR),
    .DIN     (DIN),
    .DOUT    (DOUT)
);


initial begin
    WCLK  = 0;

    forever
    begin
#5
        WCLK = ~WCLK;
    end
end

task write_memory;
input [31:0]   address;
input [31:0]   data;

begin
	wait(~WCLK);
	ADDR  = address;
	DIN   = data;
	WE    = 1;
	wait(WCLK);
	wait(~WCLK);
	WE    = 0;
	wait(WCLK);
end
endtask

task read_memory;
input [31:0]   address;

begin
	wait(~WCLK);
	ADDR  = address;
	wait(WCLK);
	wait(~WCLK);
	$display("addr=%08X, data=%08X", ADDR, DOUT);
	wait(WCLK);
end
endtask

integer i;

initial begin
    RESETN = 0;
#100
    RESETN = 1;

#10
    for(i=0; i<256; i=i+1)
    begin
	write_memory(i, i);
    end

    for(i=0; i<256; i=i+1)
    begin
	read_memory(i);
    end

#100
    $finish(0);
end

endmodule

