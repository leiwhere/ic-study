`timescale 1ns / 1ps


module clock_gen #(
    parameter CLK_PEROID  = 5.0
)(
    aclk
);

output  aclk;
reg     aclk;


// gen clock
real  peroid = CLK_PEROID;
real  h_peroid ;
initial begin
    peroid = CLK_PEROID;
    $value$plusargs("peroid=%0f", peroid);

    $display("peroid = %0f\n" , peroid);

    h_peroid = peroid / 2;
    aclk = 0;
    forever
    begin
        #h_peroid
        aclk = ~aclk;
    end
end


// delay task
task delay;
    input  [ 31:0]  c;
begin: delay
    integer i;
    for(i=0 ; i<c ; i=i+1 )
    begin
        #peroid ;
    end
end
    
endtask


endmodule





module tb_unit(
);

wire        aclk;
reg         aresetn;

reg  [31:0] bram_a_addr;
reg  [31:0] bram_a_wdata;
wire [31:0] bram_a_rdata;

reg  [31:0] bram_b_addr;
reg  [31:0] bram_b_wdata;
wire [31:0] bram_b_rdata;


clock_gen _ic(  .aclk(aclk) );

LD_c_unit _inst_unit(
    .clk(aclk),
    .resetn(aresetn),

    .bram_a_clk(aclk),
    .bram_a_addr(bram_a_addr),
    .bram_a_wdata(bram_a_wdata),
    .bram_a_rdata(bram_a_rdata),
    .bram_a_en(1'b1),
    .bram_a_we(4'hf),
    .bram_a_rstn(aresetn),

    .bram_b_clk(aclk),
    .bram_b_addr(bram_b_addr),
    .bram_b_wdata(bram_b_wdata),
    .bram_b_rdata(bram_b_rdata),
    .bram_b_en(1'b1),
    .bram_b_we(4'hf),
    .bram_b_rstn(aresetn),

    .s_mode(),
    .s_valid(),
    .s_stream(),
    .s_last(),

    .founded(),
    .vid(),
    .value()
);

task write_memory;
input [31:0]   address;
input [31:0]   data;

begin
	wait(~aclk);
	bram_a_addr  = address;
	bram_a_wdata = data;
	wait(aclk);
	wait(~aclk);
	wait(aclk);
end
endtask

task read_memory;
input [31:0]   address;
begin
	wait(~aclk);
	bram_b_addr  = address;	
	wait(aclk);
	wait(~aclk);
	wait(aclk);
	$display("read address = %08X, data = %08X", bram_b_addr, bram_b_rdata);
end
endtask

reg [31:0] i;
initial begin
	aresetn        = 0;
	_ic.delay(32);
	aresetn        = 1;
	_ic.delay(32);
	
	for(i=0; i< 64; i=i+1)
	begin
		write_memory(i*4, i);
	end

	for(i=0; i< 64; i=i+1)
	begin
		read_memory(i*4);
	end

	$finish(0);
end

endmodule
