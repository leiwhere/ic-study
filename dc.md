```
# power=106mw       max_path=112.05ns        area=33325
set target_library [ list /opt/pdk/smic40/40LL/stdcell/logic0040ll/sc9mc_base_lvt_c40/r0p1/db/sc9mc_logic0040ll_base_lvt_c40_ff_typical_min_1p21v_m40c.db]



set link_library "* $target_library"


define_design_lib WORK -path "work"


analyze -library WORK -format verilog  rtl/md5_core.v

elaborate -architecture verilog -library WORK  MD5_Transfer

check_design

set_max_area 0

compile_ultra


write -hierarchy -format verilog -output out/syn/md5_core.v


report_area                    >  out/logs/synth_area.rpt
report_cell                    >  out/logs/synth_cells.rpt
report_qor                     >  out/logs/synth_qor.rpt
report_resources               >  out/logs/synth_resoutces.rpt
report_timing -max_paths 10    >  out/logs/synth_timing.rpt
```