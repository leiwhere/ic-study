#!/usr/bin/python
# -*- coding: utf-8 -*-
import openai
from flask import Flask , jsonify , request
import json
import getopt
import sys
import os


app = Flask(__name__)

@app.route("/v1/chat/completions", methods=[ "POST"])
def chat_completions_v1():
    data = json.loads(request.get_data(as_text=True))
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo-0301",
        messages=data['messages'])
    return response


port = 7070
host = 'localhost'
key  = ''

if __name__ == "__main__":
    opts, args = getopt.getopt(sys.argv[1:], 'k:u:p:', ["host=", "port=", "key="])
    for opt, arg in opts:
        if   opt in ('-h', '--host'):
            host = arg
        elif opt in ('-p', '--port'):
            port = arg
        elif opt in ('-k' ,'--key'):
            key  = arg
        else:
            print('Usage: python chat.py [-p port] [-h host]  -k key')
            sys.exit()

    if key == '' :
        print('Usage: python chat.py [-p port] [-h host]  -k key')
        sys.exit()

    openai.api_key = key

    app.run(port=port , host=host , debug=True)